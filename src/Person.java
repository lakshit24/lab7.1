enum WeightUnits {
    KG,POUND
}

enum HeightUnits {
    CENTIMETERS,INCHES
}

class Person{
    private String name;
    private Height h;
    private Weight w;
    public double BMI;

    public Person(String name, Height h, Weight w) {
        this.name = name;
        this.h = h;
        this.w = w;
        calcualteBMI();
    }
    
    public Height getH() {
        return h;
    }
    
    public void setH(Height h) {
        this.h = h;
        calcualteBMI();
    }

    public Weight getW() {
        return w;
    }

    public void setW(Weight w) {
        this.w = w;
        calcualteBMI();
    }  
    
    
    private void calcualteBMI(){
        if(h.hu == HeightUnits.CENTIMETERS){
            double heightInMeters = h.value / 100;
            if(w.wu == WeightUnits.KG){
                BMI = w.value / (heightInMeters * heightInMeters); 
            }
            
            else{
               
                double weightInKG = w.value * 0.453592;
                BMI = weightInKG / (heightInMeters * heightInMeters);
            }
        }
       
        else{
           double heightInMeters = h.value * 2.54/100;
           if(w.wu == WeightUnits.KG){
                BMI = w.value / (heightInMeters * heightInMeters); 
            }
            else{
                
                double weightInKG = w.value * 0.453592;
                BMI = weightInKG / (heightInMeters * heightInMeters);
            }
        }
    }
}
