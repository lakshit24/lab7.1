
public class Simulation {

    public static void main(String[] args) {
        
        
        Person P1 = new Person("person1",new Height(172,HeightUnits.CENTIMETERS),new Weight(82,WeightUnits.KG));
        System.out.println(P1.BMI);
        
        Person P2 = new Person("person2",new Height(70,HeightUnits.INCHES),new Weight(152,WeightUnits.POUND));
        System.out.println(P2.BMI);
    }          
    
}